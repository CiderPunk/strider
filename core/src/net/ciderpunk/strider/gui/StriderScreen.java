package net.ciderpunk.strider.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Disposable;
import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.EntityList;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.strider.Constants;
import net.ciderpunk.strider.ents.blufor.Player;
import net.ciderpunk.strider.ents.decorations.Terrain;
import net.ciderpunk.strider.ents.redfor.Soldier;
import net.ciderpunk.strider.game.StriderGame;

/**
 * Created by Matthew on 22/08/2015.
 */
public class StriderScreen extends GameScreen implements IEntityController, Disposable {

  boolean active;
  Camera cam;
  SpriteBatch spriteBatch;
  ModelBatch modelBatch;
  Environment environment;
  StriderGame owner;
  Player player;
  int lastTerrain = -20;
  EntityList bluFor, redFor, terrain, decorations;
  static Vector2 tempV2 = new Vector2();

  float spawnRate = Constants.InitialSpawnRate;


  public boolean testBounds(float x){
    return cam.position.x  - x < Constants.BoundsLimit;
  }

  public void addDecoration(Entity ent){
    decorations.add(ent);
  }

  static Vector3 tempV3 = new Vector3();

  protected Vector3 screenToView(float x, float y) {

    tempV2.set(x, y);
    getStage().stageToScreenCoordinates(tempV2);

    cam.near = 250f;
    cam.update();
    cam.unproject(tempV3.set(tempV2.x,tempV2.y,0) );
    cam.near = 100f;
    //Gdx.app.log("info", "screen x:" + x + " y:" +  y + " projected x:" + tempV3.x + " y:" + tempV3.y + " z:" + tempV3.z);
    return tempV3;
  }

  public EntityList getRedFor(){
    return redFor;
  }


  public StriderScreen(StriderGame owner){
    super();
    modelBatch = new ModelBatch();
    spriteBatch = new SpriteBatch();

    cam = new PerspectiveCamera(75, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    cam.position.set(0f, 0f, 250f);
    cam.lookAt(0f, 0f, 0f);

    cam.near = 100f;
    cam.far = 500.0f;

    environment = new Environment();
    environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.3f, 0.2f, 0.2f, 1.0f));
    environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
    bluFor = new EntityList();
    terrain = new EntityList();
    redFor = new EntityList();
    decorations = new EntityList();

    this.addListener(new ClickListener() {

      @Override
      public boolean mouseMoved(InputEvent event, float x, float y) {
        player.pointAt(screenToView(event.getStageX(), event.getStageY()));
        return true;
      }

      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        player.pointAt(screenToView(event.getStageX(), event.getStageY()));
        player.startShoot();
        return true;
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        player.stopShoot();
      }

      @Override
      public void touchDragged(InputEvent event, float x, float y, int pointer) {
        player.pointAt(screenToView(event.getStageX(), event.getStageY()));
      }

      @Override
      public boolean keyDown(InputEvent event, int keycode) {
        switch (keycode) {
          case 29:
          case 21:
            player.speedAdjust(-1);
            break;
          case 32:
          case 22:
            player.speedAdjust(1);
        }
        return true;
      }

      @Override
      public boolean keyUp(InputEvent event, int keycode) {
        switch (keycode) {
          case 29:
          case 21:
            player.speedAdjust(1); //left, slow down
            break;
          case 32:
          case 22:
            player.speedAdjust(-1); // right speed up
        }
        return true;
      }
    });
  }



  public Player getPlayer(){
    return this.player;
  }

  static Vector3 temp = new Vector3();



  @Override
  public void act(float delta) {
    super.act(delta);
    bluFor.doUpdate(delta);
    redFor.doUpdate(delta);
    decorations.doUpdate(delta);

    cam.position.x = player.loc.x - Constants.PlayerMaxXOffset;

    int currentTerrain = MathUtils.floor((cam.position.x + Constants.DrawAhead) / Constants.TerrainSpacing);
    while (currentTerrain  > lastTerrain){
      terrain.add(Terrain.GetTerrain(this, temp.set(lastTerrain * Constants.TerrainSpacing, -156f, 0f)));
      lastTerrain++;
    }

    spawnRate -= (Constants.SpawnIncrease * delta);
    if (MathUtils.random( (int)Math.floor(delta * spawnRate)) == 0) {
      redFor.add(Soldier.GetSoldier(this, this.cam.position.x + (Constants.DrawAhead / 2f)));
    }
    terrain.doUpdate(delta);
  }

  @Override
  public void draw(Batch batch, float parentAlpha) {

    batch.end();

    cam.update();

    //draw models
    modelBatch.begin(cam);
    terrain.doDraw(modelBatch, environment);
   // bluFor.doDraw(modelBatch, environment);
    modelBatch.end();

    //draw 2D stuff
    spriteBatch.setProjectionMatrix(cam.combined);
    spriteBatch.begin();
    redFor.doDraw(spriteBatch);
    decorations.doDraw(spriteBatch);
    spriteBatch.end();

    modelBatch.begin(cam);
    ///terrain.doDraw(modelBatch, environment);
    bluFor.doDraw(modelBatch, environment);
    modelBatch.end();
    batch.begin();
  }

  public void updateSize(Stage stage){
    if (stage != null) {
      this.setWidth(stage.getWidth());
      this.setHeight(stage.getHeight());
    }
  }

  public void init() {
    player = new Player(this);
    bluFor.add(player);
    spawnRate = Constants.InitialSpawnRate;
  }

  @Override
  public void dispose() {
    spriteBatch.dispose();
    modelBatch.dispose();
  }
}
