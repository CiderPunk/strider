package net.ciderpunk.strider.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import net.ciderpunk.gamebase.game.GameManager;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.strider.ents.blufor.Player;
import net.ciderpunk.strider.ents.decorations.Ricochet;
import net.ciderpunk.strider.ents.decorations.Terrain;
import net.ciderpunk.strider.ents.redfor.Soldier;
import net.ciderpunk.strider.gui.StriderScreen;

/**
 * Created by Matthew on 22/08/2015.
 */
public class StriderGame extends GameManager {

  Table baseTable;
  StriderScreen screen;

  public StriderGame() {
    super(new ExtendViewport(640, 480));
    screen = new StriderScreen(this);
  }

  @Override
  public void loadComplete() {
    Gdx.input.setInputProcessor(stage);
    baseTable = new Table();
    baseTable.setFillParent(true);
    stage.addActor(screen);
    stage.addActor(baseTable);
    screen.init();
    stage.setKeyboardFocus(screen);
    Gdx.gl.glDepthRangef(-1f, 1.0f);
  }


  @Override
  public void draw(float dt) {
    Gdx.graphics.getGL20().glClearColor(0.2f, 0.2f, 0.3f, 0);
    //Gdx.graphics.getGL20().glClearColor(1f, 1f, 1f, 0);
    Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    super.draw(dt);
  }



  @Override
  public void preLoad(ResourceManager resMan) {
    resMan.addResourceUser(new IResourceUser[]{
            new Player.Loader(),
            new Terrain.Loader(),
            new Soldier.Loader(),
            new Ricochet.Loader(),
    });
  }

  @Override
  public void resize(int width, int height) {
    super.resize(width, height);
    screen.updateSize(stage);
  }

  @Override
  public void dispose() {
    super.dispose();
  }

  @Override
  public void postLoad(ResourceManager resMan) {

  }
}
