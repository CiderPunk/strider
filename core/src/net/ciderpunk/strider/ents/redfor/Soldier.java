package net.ciderpunk.strider.ents.redfor;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Pool;

import net.ciderpunk.gamebase.ents.AnimatedEntity;
import net.ciderpunk.gamebase.ents.Entity2D;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.gfx.AnimationBuilder;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.strider.Constants;
import net.ciderpunk.strider.ents.IKillable;
import net.ciderpunk.strider.ents.blufor.Player;
import net.ciderpunk.strider.ents.decorations.Ricochet;
import net.ciderpunk.strider.gui.StriderScreen;

/**
 * Created by Matthew on 23/08/2015.
 */
public class Soldier extends AnimatedEntity implements Pool.Poolable, IKillable {


  protected enum State{
    RunLeft,
    RunRight,
    Shoot,
    Dead,
  }

  static final float RunSpeed = 25f;
  static final float ThinkTime = 0.5f;
  static final float MinRange = 150f;
  static final float MaxRange = 250f;
  static final float FrameScale = 0.4f;




  static final Vector3 tempV3 = new Vector3();
  static final Vector3 StandDimensions = new Vector3(22f * FrameScale, 60f * FrameScale, 1f);
  static final Vector3 ShootDimensions = new Vector3(22f * FrameScale, 44f * FrameScale, 1f);



  //static final Vector3 StandDimensions = new Vector3(220f * FrameScale, 600f * FrameScale, 10f);
  //static final Vector3 ShootDimensions = new Vector3(220f * FrameScale, 440f * FrameScale, 10f);


  float nextThink = 0f;
  State state;
  //boolean alive;
  static Animation runLeft, runRight, shoot;
  static Frame  dead;
  static Sound[] screams;

  private static final Pool<Soldier> pool = new Pool<Soldier>() {
    @Override
    protected Soldier newObject() {
      return new Soldier();
    }
  };

  public Soldier(){
    super();
  }

  public static Soldier GetSoldier(IEntityController owner, float xPos){
    return pool.obtain().init(owner, xPos);
  }

  private Soldier init(IEntityController owner, float xPos) {
    this.owner = owner;
    this.loc.set(xPos,-120f);
    state = State.RunLeft;
    currentAnim = runLeft;
    return this;
  }

  @Override
  public void hurt(float damage) {
    this.state = State.Dead;
    setAnim(null);
    ((StriderScreen) owner).addDecoration(Ricochet.GetRicochet(owner, this.getVect3Loc(tempV3), Ricochet.RicochetType.Blood));
    currentFrame = dead;
    screams[MathUtils.random(9)].play(1f);
  }

  @Override
  public boolean update(float dT) {
    super.update(dT);
    if (state!= State.Dead) {
      nextThink -= dT;
      if (nextThink < 0) {
        nextThink = ThinkTime;
        Player player = ((StriderScreen) this.owner).getPlayer();
        float dist = this.loc.x - player.loc.x;
        if (dist > MaxRange || dist < 0) {
          state = State.RunLeft;
          setAnim(runLeft);
        }
        if (dist < MinRange) {
          state = State.RunRight;
          setAnim(runRight);

        } else {
          if (MathUtils.random(2) == 0) {
            state = State.Shoot;
            setAnim(shoot);
          }
        }

      }
      switch (state) {
        case RunLeft:
          this.loc.x -= RunSpeed * dT;
          break;
        case RunRight:
          this.loc.x += RunSpeed * dT;
          break;
      }
    }
    return ((StriderScreen) this.owner).testBounds(this.loc.x);
  }

  @Override
  public void reset() {

  }

  @Override
  public void cleanUp() {
    pool.free(this);
  }

  public static class Loader implements IResourceUser
  {
    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.getAssetMan().load(Constants.AtlaaPath, TextureAtlas.class);

      for (int i = 1; i < 11; i++){
        resMan.getAssetMan().load("sound/scream" + i + ".wav", Sound.class);
      }
    }

    @Override
    public void postLoad(ResourceManager resMan) {
      TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlaaPath, TextureAtlas.class);
      shoot = AnimationBuilder.buildAnim(atlas, "soldier/shoot", 0.1f, 17, 61,FrameScale );
      dead = new Frame(atlas.findRegion("soldier/dead"), 32,60);
      dead.setScale(FrameScale);
      runLeft = AnimationBuilder.buildAnim(atlas, "soldier/runleft", 0.2f, 17, 61,FrameScale );
      runRight = AnimationBuilder.buildAnim(atlas, "soldier/runright", 0.2f, 17, 61 , FrameScale);

      screams = new Sound[10];
      for (int i = 0; i < 10; i++){
        screams[i] = resMan.getAssetMan().get("sound/scream" + (i+1) + ".wav", Sound.class);
      }
    }
  }

  @Override
  public boolean rayIntersectTest(Ray ray) {
    if (state == State.Dead){
      return false;
    }
    return Intersector.intersectRayBoundsFast(ray, tempV3.set(this.loc.x, this.loc.y -5, 0), state == State.Shoot ? ShootDimensions : StandDimensions);
  }
}
