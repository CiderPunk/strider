package net.ciderpunk.strider.ents.blufor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.math.collision.Ray;
import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.Entity3D;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.strider.Constants;
import net.ciderpunk.strider.ents.IKillable;
import net.ciderpunk.strider.ents.decorations.Ricochet;
import net.ciderpunk.strider.gui.StriderScreen;

/**
 * Created by Matthew on 22/08/2015.
 */
public class Player extends Entity3D {


  static Sound shotSound;
  static Model cabModel, torsoModel;
  ModelInstance torso, cab;
  AnimationController anCon;
  float speedAdjust;
  float pointAt = 0f;
  static  Vector2 tempV2 = new Vector2();
  static  Vector3 tempV3 = new Vector3();
  Vector3 target = new Vector3();
  boolean shooting = false;
  float shootTimeout = 0f;
  Ray shootRay = new Ray();
  Plane floor = new Plane(Vector3.Y, 140f);


  public Player(IEntityController owner){
    super(owner);
    torso = new ModelInstance(torsoModel);
    cab = new ModelInstance(cabModel);

    anCon = new AnimationController(torso);
    anCon.setAnimation("torso|walk",-1 );
    this.loc.set(Constants.PlayerMaxXOffset, Constants.PlayerYOffset, 0f);

    torso.transform.setToTranslation(this.loc).rotate(0, 1, 0, 90);
    cab.transform.setToTranslation(this.loc).rotate(0, 1, 0, 90);//.rotate(0,0,1,90);
  }

  @Override
  public void draw(ModelBatch batch, Environment env) {

    batch.render(torso, env);
    batch.render(cab, env);
  }

  @Override
  public boolean update(float dT) {
    float speed = Constants.NormalSpeed  + (speedAdjust * Constants.SpeedAdjust);
    anCon.update((speed / Constants.NormalSpeed)  * dT * 1f);
    this.loc.x += speed * dT;
    torso.transform.setToTranslation(this.loc).rotate(0, 1, 0, 90);
    cab.transform.setToTranslation(this.loc).rotate(0, 1, 0, 90).rotate(1, 0, 0, pointAt);


    if (shooting){
      shootTimeout-= dT;
      if (shootTimeout < 0){
        //do shoot

        //play sound! ...hah right!
        shotSound.play(1.0f);

        shootRay.set(this.loc, target.nor());

        //test for redfor hits...
        Entity target = ((StriderScreen) owner).getRedFor().rayIntersectTest(shootRay);

        if (target instanceof IKillable){
          ((IKillable)target).hurt(100f);
        }

        if (target== null) {
          if (Intersector.intersectRayPlane(shootRay, floor, tempV3)) {
            //draw ricochet on floor if no collision
            ((StriderScreen) owner).addDecoration(Ricochet.GetRicochet(owner, tempV3, Ricochet.RicochetType.Rock));
          }
        }

        //time for next shot...
        shootTimeout += Constants.PlayerFireRate;
        //incase of long delays
        if (shootTimeout < 0){ shootTimeout = 0;}
      }


    }


    return true;
  }

  public void speedAdjust(int i) {
    speedAdjust += i;
  }


  public void pointAt(Vector3 vect) {
    //target.set(vect);
    vect.sub(this.loc);
    target.set(vect);
    //Gdx.app.log("info", "relative x:" + vect.x + " y:" + vect.y);
    pointAt =  MathUtils.clamp(-(MathUtils.atan2(vect.y, vect.x) * MathUtils.radiansToDegrees), -50f, 50f);
    //Gdx.app.log("info", "angle " + pointAt  );
    //cab.transform.setToLookAt(this.loc.add(0,80,0), vect, Vector3.Z);  )
  }


  public void stopShoot() {
    shooting = false;
  }

  public void startShoot() {
    shooting = true;


  }

  public static class Loader implements  IResourceUser {


    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.getAssetMan().load( "models/cab.g3db", Model.class);
      resMan.getAssetMan().load( "models/torso.g3db", Model.class);
      resMan.getAssetMan().load( "sound/shot.wav", Sound.class);
    }
    @Override
    public void postLoad(ResourceManager resMan) {
      torsoModel = resMan.getAssetMan().get("models/torso.g3db", Model.class);
      cabModel = resMan.getAssetMan().get("models/cab.g3db", Model.class);
      shotSound = resMan.getAssetMan().get( "sound/shot.wav", Sound.class);
    }
  }
}
