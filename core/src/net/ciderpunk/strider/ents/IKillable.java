package net.ciderpunk.strider.ents;

/**
 * Created by Matthew on 23/08/2015.
 */
public interface IKillable {
  void hurt(float damage);

}
