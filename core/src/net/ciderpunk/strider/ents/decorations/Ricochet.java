package net.ciderpunk.strider.ents.decorations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.gamebase.ents.AnimatedEntity;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.gfx.AnimationBuilder;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.strider.Constants;
import net.ciderpunk.strider.ents.blufor.Player;
import net.ciderpunk.strider.gui.StriderScreen;

/**
 * Created by Matthew on 23/08/2015.
 */
public class Ricochet  extends AnimatedEntity implements Pool.Poolable  {

  static Animation rockHit;
  static Animation bloodHit;

  private static final Pool<Ricochet> pool = new Pool<Ricochet>() {
    @Override
    protected Ricochet newObject() {
      return new Ricochet();
    }
  };


  public enum RicochetType{
    Blood,
    Rock,
  }

  @Override
  public void reset() {

  }


  @Override
  public void draw(SpriteBatch batch) {
    super.draw(batch);
  }

  public Ricochet(){
    super();
  }

  public static Ricochet GetRicochet(IEntityController owner, Vector3 pos, RicochetType type){
    return pool.obtain().init(owner, pos, type);
  }

  private Ricochet init(IEntityController owner, Vector3 pos, RicochetType type) {
    this.owner = owner;
    this.loc.set(pos.x, pos.y);
    switch(type){
      case Blood:
        this.setAnim(bloodHit);
        break;
      case Rock:
        this.setAnim(rockHit);
        break;
    }
    return this;
  }

  @Override
  protected boolean isLooping() {
    return false;
  }

  @Override
  public void cleanUp() {
    pool.free(this);
  }

  public static class Loader implements IResourceUser
  {
    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.getAssetMan().load(Constants.AtlaaPath, TextureAtlas.class);
    }

    @Override
    public void postLoad(ResourceManager resMan) {
      TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlaaPath, TextureAtlas.class);
      rockHit = AnimationBuilder.buildAnim(atlas, "ricochet/rock", 0.05f, 16, 13);
      bloodHit = AnimationBuilder.buildAnim(atlas, "ricochet/blood", 0.05f, 16, 13);
    }
  }


}
