package net.ciderpunk.strider.ents.decorations;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.gamebase.ents.Entity3D;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.strider.gui.StriderScreen;

/**
 * Created by Matthew on 22/08/2015.
 */
public class Terrain extends Entity3D implements Pool.Poolable{

  static Model terrainMdl;

  private static final Pool<Terrain>  pool = new Pool<Terrain>() {
    @Override
    protected Terrain newObject() {
      return new Terrain();
    }
  };

  public Terrain(){
    super();
    modelInstance = new ModelInstance(terrainMdl,"street1");
  }


  @Override
  public void draw(ModelBatch batch, Environment env) {
    batch.render(modelInstance,  env);
  }

  public static Terrain GetTerrain(IEntityController owner, Vector3 position){
    return pool.obtain().init(owner, position);
  }

  protected Terrain init(IEntityController owner, Vector3 position) {
    this.owner = owner;
    this.loc.set(position);
    modelInstance.transform.setToRotation(1,0,0,0);
    modelInstance.transform.setTranslation(position).rotate(0,1,0,-90);
    return this;
  }

  @Override
  public boolean update(float dT) {
    return ((StriderScreen) this.owner).testBounds(this.loc.x);
  }

  @Override
  public void reset() { }

  @Override
  public void cleanUp() {
    pool.free(this);
  }

  public static class Loader implements IResourceUser {
    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.getAssetMan().load( "models/terrain.g3db", Model.class);
    }
    @Override
    public void postLoad(ResourceManager resMan) {
      terrainMdl = resMan.getAssetMan().get("models/terrain.g3db", Model.class);
    }
  }

}
