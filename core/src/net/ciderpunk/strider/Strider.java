package net.ciderpunk.strider;

import com.badlogic.gdx.ApplicationAdapter;
import net.ciderpunk.strider.game.StriderGame;

public class Strider extends ApplicationAdapter {
  StriderGame game;

  @Override
  public void create () { game = new StriderGame(); }

  @Override
  public void render () {
    game.update();
  }

  @Override
  public void resize(int width, int height) {
    game.resize(width, height);
  }

  @Override
  public void dispose() {
    game.dispose();
    super.dispose();
  }
}
