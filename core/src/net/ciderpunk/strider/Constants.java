package net.ciderpunk.strider;

/**
 * Created by Matthew on 22/08/2015.
 */
public class Constants {
  public static final String AtlaaPath = "res.atlas";

  public static final float PlayerMaxXOffset = -180f;
  public static final float PlayerMinXOffset = 0f;
  public static final float PlayerYOffset = -60f;

  public static final float NormalSpeed = 30f;
  public static final float SpeedAdjust = 30f;

  public static final float PlayerFireRate = 0.2f;



  public static final float TerrainSpacing = 200f;
  public static final float DrawAhead= 1000f;
  public static final float BoundsLimit= 600f;


  public static final float InitialSpawnRate = 5000f;
  public static final float SpawnIncrease = 0.001f;

}


